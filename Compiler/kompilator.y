﻿%using MiniCompiler;
%namespace GardensPoint

%YYSTYPE LexicalInfo


%token ASSIGNMENT LOGICSUM LOGICMUL EQ NOTEQ LT GT LTE GTE ADD MINUS MUL DIV BITSUM BITMUL UNBITNOT UNLOGICNOT UNINTCAST UNDOUBLECAST
%token PROGRAM IF ELSE WHILE RETURN OBRACKET CBRACKET OPARENT CPARENT SEMICOLON
%token INT DOUBLE BOOL STRING VALINT VALBOOL VALDOUBLE ID
%token WRITE READ


%%

start				: programKeyword oBracketKeyword declarationSeq instructionSeq cBracketKeyword eof
					{
						MainNode = new MainStart(
							$1 as BaseProgramKeyword,
							$2 as BaseOBracketKeyword,
							$3 as BaseDeclarationSequence,
							$4 as BaseInstructionSequence,
							$5 as BaseCBracketKeyword,
							$6 as BaseEof
							);
					}
					;

/* declarations */

declaration			: valueType id semicolonKeyword
					{
						$$ = new DeclarationDeclaration(
							$1 as BaseValueType,
							$2 as BaseIdKeyword,
							$3 as BaseSemicolonKeyword
							);
					}
					| valueType id error
					{
						$$ = new SyntaxErrorMissingSemicolonDeclaration(
							$2 as LexicalInfo
							);
					}
					;

declarationSeq		:
					{
						$$ = new EmptyDeclarationSequence(-1,-1,"");
					}
					| declarationSeq declaration
					{
						$$ = new DeclarationDeclarationSequence(
							$1 as BaseDeclarationSequence,
							$2 as BaseDeclaration
							);
					}
					;

valueType			: INT
					{
						$$ = new IntValueType($1);
					}
					| DOUBLE
					{
						$$ = new DoubleValueType($1);
					}
					| BOOL
					{
						$$ = new BoolValueType($1);
					}
					;


/* expressions */

assignmentExp		: logicalExp
					{
						$$ = new LogicalExpressionAssignmentExpression(
							$1 as BaseLogicalExpression
							);
					}
					| id assignmentOp assignmentExp
					{
						$$ = new IdAssignmentExpression(
							$1 as BaseIdKeyword,
							$2 as BaseAssignmentOperator,
							$3 as BaseAssignmentExpression
							);
					}
					;

assignmentOp		: ASSIGNMENT
					{
						$$ = new AssignmentAssignmentOperator($1);
					}
					;

logicalExp			: relationExp
					{
						$$ = new RelationExpressionLogicalExpression(
							$1 as BaseRelationExpression
							);
					}
					| logicalExp logicalOp relationExp
					{
						$$ = new LogicalExpressionLogicalExpression(
							$1 as BaseLogicalExpression,
							$2 as BaseLogicalOperator,
							$3 as BaseRelationExpression
							);
					}
					;

logicalOp			: LOGICSUM
					{
						$$ = new LogicalSumLogicalOperator($1);
					}
					| LOGICMUL
					{
						$$ = new LogicalMulLogicalOperator($1);
					}
					;

relationExp			: additiveExp
					{
						$$ = new AdditiveExpressionRelationExpression(
							$1 as BaseAdditiveExpression
							);
					}
					| relationExp relationOp additiveExp
					{
						$$ = new RelationExpressionRelationExpression(
							$1 as BaseRelationExpression,
							$2 as BaseRelationOperator,
							$3 as BaseAdditiveExpression
							);
					}
					;

relationOp			: GT
					{
						$$ = new GreaterThanRelationOperator($1);
					}
					| LT
					{
						$$ = new LessThanRelationOperator($1);
					}
					| GTE
					{
						$$ = new GreaterThanEqualRelationOperator($1);
					}
					| LTE
					{
						$$ = new LessThanEqualRelationOperator($1);
					}
					| NOTEQ
					{
						$$ = new NotEqualRelationOperator($1);
					}
					| EQ
					{
						$$ = new EqualRelationOperator($1);
					}
					;

additiveExp			: multiplicativeExp
					{
						$$ = new MultiplicativeExpressionAdditiveExpression(
							$1 as BaseMultiplicativeExpression
							);
					}
					| additiveExp additiveOp multiplicativeExp
					{
						$$ = new AdditiveExpressionAdditiveExpression(
							$1 as BaseAdditiveExpression,
							$2 as BaseAdditiveOperator,
							$3 as BaseMultiplicativeExpression
							);
					}
					;

additiveOp			: ADD
					{
						$$ = new AddAdditiveOperator($1);
					}
					| MINUS
					{
						$$ = new SubAdditiveOperator($1);
					}
					;

multiplicativeExp	: bitwiseExp
					{
						$$ = new BitwiseExpressionMultiplicativeExpression(
							$1 as BaseBitwiseExpression
							);
					}
					| multiplicativeExp multiplicativeOp bitwiseExp
					{
						$$ = new MultiplicativeExpressionMultiplicativeExpression(
							$1 as BaseMultiplicativeExpression,
							$2 as BaseMultiplicativeOperator,
							$3 as BaseBitwiseExpression
							);
					}
					;

multiplicativeOp	: MUL
					{
						$$ = new MultiplyMultiplicativeOperator($1);
					}
					| DIV
					{
						$$ = new DivideMultiplicativeOperator($1);
					}
					;

bitwiseExp			: unaryExp
					{
						$$ = new UnaryExpressionBitwiseExpression(
							$1 as BaseUnaryExpression
							);
					}
					| bitwiseExp bitwiseOp unaryExp
					{
						$$ = new BitwiseExpressionBitwiseExpression(
							$1 as BaseBitwiseExpression,
							$2 as BaseBitwiseOperator,
							$3 as BaseUnaryExpression);
					}
					;

bitwiseOp			: BITSUM
					{
						$$ = new BitwiseSumBitwiseOperator($1);
					}
					| BITMUL
					{
						$$ = new BitwiseMulBitwiseOperator($1);
					}
					;

unaryExp			: endExp
					{
						$$ = new EndExpressionUnaryExpression(
							$1 as BaseEndExpression
							);
					}
					| unaryOp unaryExp
					{
						$$ = new UnaryExpressionUnaryExpression(
							$1 as BaseUnaryOperator,
							$2 as BaseUnaryExpression);
					}
					;

unaryOp				: MINUS
					{
						$$ = new SubUnaryOperator($1);
					}
					| UNBITNOT
					{
						$$ = new BitwiseNotUnaryOperator($1);
					}
					| UNLOGICNOT
					{
						$$ = new LogicNotUnaryOperator($1);
					}
					| UNINTCAST
					{
						$$ = new IntCastUnaryOperator($1);
					}
					| UNDOUBLECAST
					{
						$$ = new DoubleCastUnaryOperator($1);
					}
					;


endExp				: oParentKeyword assignmentExp cParentKeyword
					{
						$$ = new ParenthesisEndExpression(
							$1 as BaseOParentKeyword,
							$2 as BaseAssignmentExpression,
							$3 as BaseCParentKeyword
							);
					}
					| value
					{
						$$ = new ValueEndExpression(
							$1 as BaseValue
							);
					}
					| id
					{
						$$ = new IdEndExpression(
							$1 as BaseIdKeyword
							);
					}
					;

value				: VALINT
					{
						$$ = new IntValue($1);
					}
					| VALBOOL
					{
						$$ = new BoolValue($1);
					}
					| VALDOUBLE
					{
						$$ = new DoubleValue($1);
					}
					;

/* instructions */
instruction			: blockInstr
					{
						$$ = new BlockInstruction(
							$1 as BaseBlockInstruction
							);
					}
					| expInstr
					{
						$$ = new ExpressionInstruction(
							$1 as BaseExpressionInstruction
							);
					}
					| ifInstr
					{
						$$ = new IfInstruction(
							$1 as BaseIfInstruction
							);
					}
					| whileInstr
					{
						$$ = new WhileInstruction(
							$1 as BaseWhileInstruction
							);
					}
					| readInstr
					{
						$$ = new ReadInstruction(
							$1 as BaseReadInstruction
							);
					}
					| writeInstr
					{
						$$ = new WriteInstruction(
							$1 as BaseWriteInstruction
							);
					}
					| returnInstr
					{
						$$ = new ReturnInstruction(
							$1 as BaseReturnInstruction
							);
					}
					| semicolonKeyword
					{
						$$ = new SyntaxErrorDanglingSemicolonInstruction(
							$1 as LexicalInfo
							);
					}
					;

instructionSeq		:
					{
						$$ = new EmptyInstructionSequence(-1,-1,"");
					}
					| instructionSeq instruction
					{
						$$ = new InstructionInstructionSequence(
							$1 as BaseInstructionSequence,
							$2 as BaseInstruction
							);
					}
					| EOF
					{
						ErrorMessages.AddSyntaxErrorEOF($1.Line);
						ErrorMessages.PrintErrors();
						YYABORT;
					}
					;

blockInstr			: oBracketKeyword instructionSeq cBracketKeyword
					{
						$$ = new BlockBlockInstruction(
							$1 as BaseOBracketKeyword,
							$2 as BaseInstructionSequence,
							$3 as BaseCBracketKeyword
							);
					}
					;

expInstr			: assignmentExp semicolonKeyword
					{
						$$ = new ExpressionExpressionInstruction(
							$1 as BaseAssignmentExpression,
							$2 as BaseSemicolonKeyword
							);
					}
					| error
					{
						$$ = new SyntaxErrorMissingSemicolonExpressionInstruction(
							$1 as LexicalInfo
							);
					}
					;

ifInstr				: ifKeyword oParentKeyword assignmentExp cParentKeyword instruction elseKeyword instruction
					{
						$$ = new IfElseIfInstruction(
							$1 as BaseIfKeyword,
							$2 as BaseOParentKeyword,
							$3 as BaseAssignmentExpression,
							$4 as BaseCParentKeyword,
							$5 as BaseInstruction,
							$6 as BaseElseKeyword,
							$7 as BaseInstruction
							);
					}
					| ifKeyword oParentKeyword assignmentExp cParentKeyword instruction
					{
						$$ = new IfIfInstruction(
							$1 as BaseIfKeyword,
							$2 as BaseOParentKeyword,
							$3 as BaseAssignmentExpression,
							$4 as BaseCParentKeyword,
							$5 as BaseInstruction
							);
					}
					;

whileInstr			: whileKeyword oParentKeyword assignmentExp cParentKeyword instruction
					{
						$$ = new WhileWhileInstruction(
							$1 as BaseWhileKeyword,
							$2 as BaseOParentKeyword,
							$3 as BaseAssignmentExpression,
							$4 as BaseCParentKeyword,
							$5 as BaseInstruction);
					}
					;


readInstr			: readKeyword id semicolonKeyword
					{
						$$ = new ReadReadInstruction(
							$1 as BaseReadKeyword,
							$2 as BaseIdKeyword,
							$3 as BaseSemicolonKeyword
							);
					}
					;

writeInstr			: writeKeyword string semicolonKeyword
					{
						$$ = new WriteStringWriteInstruction(
							$1 as BaseWriteKeyword,
							$2 as BaseStringKeyword,
							$3 as BaseSemicolonKeyword
							);
					}
					| writeKeyword assignmentExp semicolonKeyword
					{
						$$ = new WriteIdWriteInstruction(
							$1 as BaseWriteKeyword,
							$2 as BaseAssignmentExpression,
							$3 as BaseSemicolonKeyword
							);
					}
					;

returnInstr			: returnKeyword semicolonKeyword
					{
						$$ = new ReturnReturnInstruction(
							$1 as BaseReturnKeyword,
							$2 as BaseSemicolonKeyword
							);
					}
					;


whileKeyword		: WHILE
					{
						$$ = new WhileKeyword($1);
					}
					;

ifKeyword			: IF
					{
						$$ = new IfKeyword($1);
					}
					;

oParentKeyword		: OPARENT
					{
						$$ = new OParentKeyword($1);
					}
					;

cParentKeyword		: CPARENT
					{
						$$ = new CParentKeyword($1);
					}
					;

elseKeyword			: ELSE
					{
						$$ = new ElseKeyword($1);
					}
					;

semicolonKeyword	: SEMICOLON
					{
						$$ = new SemicolonKeyword($1);
					}
					;

string				: STRING
					{
						$$ = new StringKeyword($1);
					}
					;

id					: ID
					{
						$$ = new IdKeyword($1);
					}
					;

returnKeyword		: RETURN
					{
						$$ = new ReturnKeyword($1);
					}
					;

oBracketKeyword		: OBRACKET
					{
						$$ = new OBracketKeyword($1);
					}
					;

cBracketKeyword		: CBRACKET
					{
						$$ = new CBracketKeyword($1);
					}
					;

programKeyword		: PROGRAM
					{
						$$ = new ProgramKeyword($1);
					}
					;

readKeyword			: READ
					{
						$$ = new ReadKeyword($1);
					}
					;

writeKeyword		: WRITE
					{
						$$ = new WriteKeyword($1);
					}
					;

eof					: EOF
					{
						$$ = new Eof($1);
					}
					;

%%

public MainStart MainNode {get;set;}

public Parser(Scanner scanner, MainStart MainNode) : base(scanner)
{
	this.MainNode = MainNode;
}

