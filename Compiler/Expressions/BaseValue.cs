﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseValue : BaseTypeNode
    {
        public BaseValue(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class DoubleValue : BaseValue
    {
        double Value => double.Parse(Text, CultureInfo.InvariantCulture);

        public DoubleValue(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }

        public override string Assembly()
        {
            return $"ldc.r8 {Value.ToString(CultureInfo.InvariantCulture)}\n";
        }

        protected override void UpdateTypeEnum()
        {
            typeEnum = TypeEnum.Double;
        }
    }
    public class IntValue : BaseValue
    {
        int Value => int.Parse(Text, CultureInfo.InvariantCulture);


        public IntValue(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {

        }
        public override string Assembly()
        {
            return $"ldc.i4 {Value}\n";
        }

        protected override void UpdateTypeEnum()
        {
            typeEnum = TypeEnum.Int;
        }
    }

    public class BoolValue : BaseValue
    {
        bool Value => bool.Parse(Text);

        public BoolValue(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }
        public override string Assembly()
        {
            return Value ? "ldc.i4.1\n" : "ldc.i4.0\n";
        }
        protected override void UpdateTypeEnum()
        {
            typeEnum = TypeEnum.Bool;
        }
    }


}
