﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseUnaryExpression : BaseTypeNode
    {
        public BaseUnaryExpression(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class EndExpressionUnaryExpression : BaseUnaryExpression
    {
        BaseEndExpression EndExpression { get; set; }

        public EndExpressionUnaryExpression(
            BaseEndExpression EndExpression
            ) : base(EndExpression.Line, EndExpression.Col, EndExpression.Text)
        {
            this.EndExpression = EndExpression;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            if (GetTypeEnum() != TypeEnum.Invalid)
            {
                code += EndExpression.Assembly();
            }
            return code;
        }

        protected override void UpdateTypeEnum()
        {
            typeEnum = EndExpression.GetTypeEnum();
        }
    }

    public class UnaryExpressionUnaryExpression : BaseUnaryExpression
    {
        BaseUnaryOperator UnaryOperator { get; set; }
        BaseUnaryExpression UnaryExpression { get; set; }

        public UnaryExpressionUnaryExpression(
            BaseUnaryOperator UnaryOperator,
            BaseUnaryExpression UnaryExpression
            ) : base(UnaryOperator.Line, UnaryOperator.Col, UnaryOperator.Text)
        {
            this.UnaryOperator = UnaryOperator;
            this.UnaryExpression = UnaryExpression;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            if (GetTypeEnum() != TypeEnum.Invalid)
            {
                code += UnaryExpression.Assembly();
                code += UnaryOperator.Assembly();
            }
            return code;
        }

        protected override void UpdateTypeEnum()
        {
            typeEnum = UnaryOperator.GetResultType(UnaryExpression);
        }
    }
}
