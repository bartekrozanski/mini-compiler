﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseAdditiveExpression : BaseTypeNode
    {
        public BaseAdditiveExpression(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class MultiplicativeExpressionAdditiveExpression : BaseAdditiveExpression
    {
        BaseMultiplicativeExpression MultiplicativeExpression { get; set; }
        public MultiplicativeExpressionAdditiveExpression(
            BaseMultiplicativeExpression multiplicativeExpression
            ) : base(multiplicativeExpression.Line, multiplicativeExpression.Col, multiplicativeExpression.Text)
        {
            MultiplicativeExpression = multiplicativeExpression;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            if(GetTypeEnum() != TypeEnum.Invalid)
            {
                code += MultiplicativeExpression.Assembly();
            }
            return code;
        }

        protected override void UpdateTypeEnum()
        {
            typeEnum = MultiplicativeExpression.GetTypeEnum();
        }
    }

    public class AdditiveExpressionAdditiveExpression : BaseAdditiveExpression
    {
        BaseAdditiveExpression AdditiveExpression { get; set; }
        BaseAdditiveOperator AdditiveOperator { get; set; }
        BaseMultiplicativeExpression MultiplicativeExpression { get; set; }
        public AdditiveExpressionAdditiveExpression(
            BaseAdditiveExpression additiveExpression,
            BaseAdditiveOperator additiveOperator,
            BaseMultiplicativeExpression multiplicativeExpression
            ) : base(additiveOperator.Line, additiveOperator.Col, additiveOperator.Text)
        {
            AdditiveExpression = additiveExpression;
            AdditiveOperator = additiveOperator;
            MultiplicativeExpression = multiplicativeExpression;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            if(GetTypeEnum() != TypeEnum.Invalid)
            {
                code += AdditiveExpression.Assembly();
                code += AdditiveOperator.arg1CastCommand;
                code += MultiplicativeExpression.Assembly();
                code += AdditiveOperator.arg2CastCommand;
                code += AdditiveOperator.Assembly();
            }
            return code;
        }

        protected override void UpdateTypeEnum()
        {
            typeEnum = AdditiveOperator.GetResultType(AdditiveExpression, MultiplicativeExpression);
        }
    }
}
