﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseBitwiseExpression : BaseTypeNode
    {
        public BaseBitwiseExpression(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class UnaryExpressionBitwiseExpression : BaseBitwiseExpression
    {
        BaseUnaryExpression UnaryExpression { get; set; }
        public UnaryExpressionBitwiseExpression(
            BaseUnaryExpression UnaryExpression
            ) : base(UnaryExpression.Line, UnaryExpression.Col, UnaryExpression.Text)
        {
            this.UnaryExpression = UnaryExpression;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            if (GetTypeEnum() != TypeEnum.Invalid)
            {
                code += UnaryExpression.Assembly();
            }
            return code;
        }

        protected override void UpdateTypeEnum()
        {
            typeEnum = UnaryExpression.GetTypeEnum();
        }
    }

    public class BitwiseExpressionBitwiseExpression : BaseBitwiseExpression
    {
        BaseBitwiseExpression BitwiseExpression { get; set; }
        BaseBitwiseOperator BitwiseOperator { get; set; }
        BaseUnaryExpression UnaryExpression { get; set; }
        public BitwiseExpressionBitwiseExpression(
            BaseBitwiseExpression BitwiseExpression,
            BaseBitwiseOperator BitwiseOperator,
            BaseUnaryExpression UnaryExpression
            ) : base(BitwiseOperator.Line, BitwiseOperator.Col, BitwiseOperator.Text)
        {
            this.BitwiseExpression = BitwiseExpression;
            this.BitwiseOperator = BitwiseOperator;
            this.UnaryExpression = UnaryExpression;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            if (GetTypeEnum() != TypeEnum.Invalid)
            {
                code += BitwiseExpression.Assembly();
                code += UnaryExpression.Assembly();
                code += BitwiseOperator.Assembly();
            }
            return code;
        }

        protected override void UpdateTypeEnum()
        {
            typeEnum = BitwiseOperator.GetResultType(BitwiseExpression, UnaryExpression);
        }
    }
}
