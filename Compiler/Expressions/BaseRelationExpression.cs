﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseRelationExpression : BaseTypeNode
    {
        public BaseRelationExpression(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class AdditiveExpressionRelationExpression : BaseRelationExpression
    {
        BaseAdditiveExpression AdditiveExpression { get; set; }
        public AdditiveExpressionRelationExpression(BaseAdditiveExpression 
            AdditiveExpression
            )
            : base(AdditiveExpression.Line, AdditiveExpression.Col, AdditiveExpression.Text)
        {
            this.AdditiveExpression = AdditiveExpression;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            if(GetTypeEnum() != TypeEnum.Invalid)
            {
                code += AdditiveExpression.Assembly();
            }
            return code;
        }

        protected override void UpdateTypeEnum()
        {
            typeEnum = AdditiveExpression.GetTypeEnum();
        }
    }

    public class RelationExpressionRelationExpression : BaseRelationExpression
    {
        BaseRelationExpression RelationExpression { get; set; }
        BaseRelationOperator RelationOperator { get; set; }
        BaseAdditiveExpression AdditiveExpression { get; set; }

        public RelationExpressionRelationExpression(
            BaseRelationExpression relationExpression,
            BaseRelationOperator relationOperator,
            BaseAdditiveExpression additiveExpression
            ) : base(relationOperator.Line, relationOperator.Col, relationOperator.Text)
        {
            RelationExpression = relationExpression;
            RelationOperator = relationOperator;
            AdditiveExpression = additiveExpression;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            if(GetTypeEnum() != TypeEnum.Invalid)
            {
                code += RelationExpression.Assembly();
                code += RelationOperator.arg1CastCommand;
                code += AdditiveExpression.Assembly();
                code += RelationOperator.arg2CastCommand;
                code += RelationOperator.Assembly();
            }
            return code;
        }

        protected override void UpdateTypeEnum()
        {
            typeEnum = RelationOperator.GetResultType(RelationExpression, AdditiveExpression);
        }
    }
}
