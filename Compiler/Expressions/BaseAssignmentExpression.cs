﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseAssignmentExpression : BaseTypeNode
    {
        public BaseAssignmentExpression(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class LogicalExpressionAssignmentExpression : BaseAssignmentExpression
    {
        BaseLogicalExpression LogicalExpression { get; set; }

        public LogicalExpressionAssignmentExpression(
            BaseLogicalExpression LogicalExpression
            ) 
            : base(LogicalExpression.Line, LogicalExpression.Col, LogicalExpression.Text)
        {
            this.LogicalExpression = LogicalExpression;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            {
                code += LogicalExpression.Assembly();
            }
            return code;
        }

        protected override void UpdateTypeEnum()
        {
            typeEnum = LogicalExpression.GetTypeEnum();
        }
    }

    public class IdAssignmentExpression : BaseAssignmentExpression
    {
        BaseIdKeyword IdKeyword { get; set; }
        BaseAssignmentOperator AssignmentOperator { get; set; }
        BaseAssignmentExpression AssignmentExpression {get;set;}
        public IdAssignmentExpression(BaseIdKeyword idKeyword,
            BaseAssignmentOperator assignmentOperator,
            BaseAssignmentExpression assignmentExpression) 
            : base(assignmentOperator.Line, assignmentOperator.Col, assignmentOperator.Text)
        {
            IdKeyword = idKeyword;
            AssignmentOperator = assignmentOperator;
            AssignmentExpression = assignmentExpression;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            if(GetTypeEnum() != TypeEnum.Invalid)
            {
                code += AssignmentExpression.Assembly();
                code += AssignmentOperator.arg2CastCommand;
                code += $"{AssignmentOperator.Assembly()} {IdKeyword.IntId}\n";

                //always after assignment operation, leave recently assigned value on stack to allow multpile assignments in expression instruction;
                code += IdKeyword.Assembly();
            }
            return code;
        }

        protected override void UpdateTypeEnum()
        {
            typeEnum = AssignmentOperator.GetResultType(IdKeyword, AssignmentExpression);
        }
    }
}
