﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseLogicalExpression : BaseTypeNode
    {
        public BaseLogicalExpression(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class RelationExpressionLogicalExpression : BaseLogicalExpression
    {
        BaseRelationExpression RelationExpression { get; set; }
        public RelationExpressionLogicalExpression(BaseRelationExpression RelationExpression) 
            : base(RelationExpression.Line, RelationExpression.Col, RelationExpression.Text)
        {
            this.RelationExpression = RelationExpression;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            if(GetTypeEnum() != TypeEnum.Invalid)
            {
                code += RelationExpression.Assembly();
            }
            return code;
        }

        protected override void UpdateTypeEnum()
        {
            typeEnum = RelationExpression.GetTypeEnum();
        }
    }

    public class LogicalExpressionLogicalExpression : BaseLogicalExpression
    {
        BaseLogicalExpression LogicalExpression { get; set; }
        BaseLogicalOperator LogicalOperator { get; set; }
        BaseRelationExpression RelationExpression { get; set; }
        public LogicalExpressionLogicalExpression(BaseLogicalExpression logicalExpression,
            BaseLogicalOperator logicalOperator,
            BaseRelationExpression relationExpression) :
            base(logicalOperator.Line, logicalOperator.Col, logicalOperator.Text)
        {
            LogicalExpression = logicalExpression;
            LogicalOperator = logicalOperator;
            RelationExpression = relationExpression;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            if(GetTypeEnum() != TypeEnum.Invalid)
            {
                code += LogicalOperator.AssemblyShortCircuitCode(LogicalExpression, RelationExpression);
            }
            return code;
        }

        protected override void UpdateTypeEnum()
        {
            typeEnum = LogicalOperator.GetResultType(LogicalExpression, RelationExpression);
        }
    }
}
