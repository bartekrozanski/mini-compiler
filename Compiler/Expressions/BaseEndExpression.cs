﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseEndExpression : BaseTypeNode
    {
        public BaseEndExpression(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class ParenthesisEndExpression : BaseEndExpression
    {
        BaseOParentKeyword OParentKeyword { get; set; }
        BaseAssignmentExpression AssignmentExpression { get; set; }
        BaseCParentKeyword CParentKeyword { get; set; }

        public ParenthesisEndExpression(
            BaseOParentKeyword OParentKeyword,
            BaseAssignmentExpression AssignmentExpression,
            BaseCParentKeyword CParentKeyword
            ) : base(AssignmentExpression.Line, AssignmentExpression.Col, AssignmentExpression.Text)
        {
            this.OParentKeyword = OParentKeyword;
            this.AssignmentExpression = AssignmentExpression;
            this.CParentKeyword = CParentKeyword;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            if (GetTypeEnum() != TypeEnum.Invalid)
            {
                code += AssignmentExpression.Assembly();
            }
            return code;
        }

        protected override void UpdateTypeEnum()
        {
            typeEnum = AssignmentExpression.GetTypeEnum();
        }
    }

    public class ValueEndExpression : BaseEndExpression
    {
        BaseValue Value { get; set; }


        public ValueEndExpression(
            BaseValue Value
            ) : base(Value.Line, Value.Col, Value.Text)
        {
            this.Value = Value;
        }
        public override string Assembly()
        {
            string code = string.Empty;
            if (GetTypeEnum() != TypeEnum.Invalid)
            {
                code += Value.Assembly();
            }
            return code;
        }
        protected override void UpdateTypeEnum()
        {
            typeEnum = Value.GetTypeEnum();
        }
    }
    public class IdEndExpression : BaseEndExpression
    {
        BaseIdKeyword IdKeyword { get; set; }


        public IdEndExpression(
            BaseIdKeyword IdKeyword
            ) : base(IdKeyword.Line, IdKeyword.Col, IdKeyword.Text)
        {
            this.IdKeyword = IdKeyword;
        }
        public override string Assembly()
        {
            string code = string.Empty;
            if(GetTypeEnum() != TypeEnum.Invalid)
            {
                code += IdKeyword.Assembly();
            }
            return code;
        }

        protected override void UpdateTypeEnum()
        {
            typeEnum =  IdKeyword.GetTypeEnum();
        }
    }
}
