﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseMultiplicativeExpression : BaseTypeNode
    {
        public BaseMultiplicativeExpression(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class BitwiseExpressionMultiplicativeExpression : BaseMultiplicativeExpression
    {
        BaseBitwiseExpression BitwiseExpression { get; set; }
        public BitwiseExpressionMultiplicativeExpression(
            BaseBitwiseExpression bitwiseExpression
            ) : base(bitwiseExpression.Line, bitwiseExpression.Col, bitwiseExpression.Text)
        {
            BitwiseExpression = bitwiseExpression;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            if(GetTypeEnum() != TypeEnum.Invalid)
            {
                code += BitwiseExpression.Assembly();
            }
            return code;
        }

        protected override void UpdateTypeEnum()
        {
            typeEnum = BitwiseExpression.GetTypeEnum();
        }
    }

    public class MultiplicativeExpressionMultiplicativeExpression : BaseMultiplicativeExpression
    {
        BaseMultiplicativeExpression MultiplicativeExpression { get; set; }
        BaseMultiplicativeOperator MultiplicativeOperator { get; set; }
        BaseBitwiseExpression BitwiseExpression { get; set; }
        public MultiplicativeExpressionMultiplicativeExpression(
            BaseMultiplicativeExpression multiplicativeExpression,
            BaseMultiplicativeOperator multiplicativeOperator,
            BaseBitwiseExpression bitwiseExpression
            ) : base(multiplicativeOperator.Line, multiplicativeExpression.Col, multiplicativeExpression.Text)
        {
            MultiplicativeExpression = multiplicativeExpression;
            MultiplicativeOperator = multiplicativeOperator;
            BitwiseExpression = bitwiseExpression;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            if(GetTypeEnum() != TypeEnum.Invalid)
            {
                code += MultiplicativeExpression.Assembly();
                code += MultiplicativeOperator.arg1CastCommand;
                code += BitwiseExpression.Assembly();
                code += MultiplicativeOperator.arg2CastCommand;
                code += MultiplicativeOperator.Assembly();
            }
            return code;
        }

        protected override void UpdateTypeEnum()
        {
            typeEnum = MultiplicativeOperator.GetResultType(MultiplicativeExpression, BitwiseExpression);
        }
    }
}
