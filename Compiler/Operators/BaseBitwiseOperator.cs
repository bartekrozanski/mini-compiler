﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseBitwiseOperator : TwoArgumentOperator
    {
        public BaseBitwiseOperator(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class BitwiseSumBitwiseOperator : BaseBitwiseOperator
    {
        public BitwiseSumBitwiseOperator(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }

        public override string Assembly()
        {
            return "or\n";
        }

        public override TypeEnum GetResultType(BaseTypeNode arg1, BaseTypeNode arg2)
        {
            var arg1Type = arg1.GetTypeEnum();
            var arg2Type = arg2.GetTypeEnum();
            if (arg1Type == TypeEnum.Invalid || arg2Type == TypeEnum.Invalid) return TypeEnum.Invalid;
            if (arg1Type == TypeEnum.Int && arg2Type == TypeEnum.Int) return TypeEnum.Int;
            ErrorMessages.AddBinaryTypeMismatch("|", "(int) (int)", $"({arg1Type.ToString()}) ({arg2Type.ToString()})", Line);
            return TypeEnum.Invalid;
        }
    }

    public class BitwiseMulBitwiseOperator : BaseBitwiseOperator
    {
        public BitwiseMulBitwiseOperator(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }
        public override string Assembly()
        {
            return "and\n";
        }

        public override TypeEnum GetResultType(BaseTypeNode arg1, BaseTypeNode arg2)
        {
            var arg1Type = arg1.GetTypeEnum();
            var arg2Type = arg2.GetTypeEnum();
            if (arg1Type == TypeEnum.Invalid || arg2Type == TypeEnum.Invalid) return TypeEnum.Invalid;
            if (arg1Type == TypeEnum.Int && arg2Type == TypeEnum.Int) return TypeEnum.Int;
            ErrorMessages.AddBinaryTypeMismatch("&", "(int) (int)", $"({arg1Type.ToString()}) ({arg2Type.ToString()})", Line);
            return TypeEnum.Invalid;
        }
    }
}
