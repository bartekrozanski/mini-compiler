﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseUnaryOperator : OneArgumentOperator
    {
        public BaseUnaryOperator(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class SubUnaryOperator : BaseUnaryOperator
    {

        public SubUnaryOperator(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }

        public override string Assembly()
        {
            return "neg\n";
        }

        public override TypeEnum GetResultType(BaseTypeNode arg)
        {
            var argType = arg.GetTypeEnum();
            if (argType == TypeEnum.Invalid) return argType;
            if (argType == TypeEnum.Double || argType == TypeEnum.Int) return argType;
            ErrorMessages.AddUnaryTypeMismatch("-", "(int|double)", argType.ToString(), Line);
            return TypeEnum.Invalid;
        }
    }

    public class BitwiseNotUnaryOperator : BaseUnaryOperator
    {

        public BitwiseNotUnaryOperator(
                    LexicalInfo lexicalInfo
                    ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }

        public override string Assembly()
        {
            return "not\n";
        }

        public override TypeEnum GetResultType(BaseTypeNode arg)
        {
            var argType = arg.GetTypeEnum();
            if (argType == TypeEnum.Invalid) return argType;
            if (argType == TypeEnum.Int) return argType;
            ErrorMessages.AddUnaryTypeMismatch("~", "(int)", argType.ToString(), Line);
            return TypeEnum.Invalid;
        }
    }

    public class LogicNotUnaryOperator : BaseUnaryOperator
    {

        public LogicNotUnaryOperator(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }
        public override string Assembly()
        {
            return "ldc.i4.0\nceq\n";
        }

        public override TypeEnum GetResultType(BaseTypeNode arg)
        {
            var argType = arg.GetTypeEnum();
            if (argType == TypeEnum.Invalid) return argType;
            if (argType == TypeEnum.Bool) return argType;
            ErrorMessages.AddUnaryTypeMismatch("!", "(bool)", argType.ToString(), Line);
            return TypeEnum.Invalid;
        }
    }

    public class IntCastUnaryOperator : BaseUnaryOperator
    {

        public IntCastUnaryOperator(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }

        public override string Assembly()
        {
            return "conv.i4\n";
        }

        public override TypeEnum GetResultType(BaseTypeNode arg)
        {
            var argType = arg.GetTypeEnum();
            if (argType == TypeEnum.Invalid) return argType;
            if (argType == TypeEnum.Int || argType == TypeEnum.Double || argType == TypeEnum.Bool) return TypeEnum.Int;
            ErrorMessages.AddUnaryTypeMismatch("(int)", "(int|double|bool)", argType.ToString(), Line);
            return TypeEnum.Invalid;
        }
    }

    public class DoubleCastUnaryOperator : BaseUnaryOperator
    {
        public DoubleCastUnaryOperator(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }

        public override string Assembly()
        {
            return "conv.r8\n";
        }

        public override TypeEnum GetResultType(BaseTypeNode arg)
        {
            var argType = arg.GetTypeEnum();
            if (argType == TypeEnum.Invalid) return argType;
            if (argType == TypeEnum.Int || argType == TypeEnum.Double || argType == TypeEnum.Bool) return TypeEnum.Double;
            ErrorMessages.AddUnaryTypeMismatch("(double)", "(int|double|bool)", argType.ToString(), Line);
            return TypeEnum.Invalid;
        }
    }

}
