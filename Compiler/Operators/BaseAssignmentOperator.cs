﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseAssignmentOperator : TwoArgumentOperator
    {
        public BaseAssignmentOperator(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class AssignmentAssignmentOperator : BaseAssignmentOperator
    {
        public AssignmentAssignmentOperator(LexicalInfo lexicalInfo)
            : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }

        public override string Assembly()
        {
            return "stloc";
        }

        public override TypeEnum GetResultType(BaseTypeNode arg1, BaseTypeNode arg2)
        {
            var arg1Type = arg1.GetTypeEnum();
            var arg2Type = arg2.GetTypeEnum();
            if (arg1Type == TypeEnum.Invalid || arg2Type == TypeEnum.Invalid) return TypeEnum.Invalid;
            if (arg1Type == TypeEnum.Int && arg2Type == TypeEnum.Int) return TypeEnum.Int;
            if (arg1Type == TypeEnum.Bool && arg2Type == TypeEnum.Bool) return TypeEnum.Bool;
            if (arg1Type == TypeEnum.Double && (arg2Type == TypeEnum.Int || arg2Type == TypeEnum.Bool))
            {
                arg2CastCommand = "conv.r8\n";
                return TypeEnum.Double;
            }
            if (arg1Type == TypeEnum.Double && arg2Type == TypeEnum.Double) return TypeEnum.Double;
            else
            {
                ErrorMessages.AddBinaryTypeMismatch("=", "((double)|(int)|(bool)) ((double|int|bool)|(int)|(bool))", $"({arg1Type.ToString()}) ({arg2Type.ToString()})", Line);
                return TypeEnum.Invalid;
            }
        }
    }
}
