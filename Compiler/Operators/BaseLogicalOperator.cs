﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseLogicalOperator : TwoArgumentOperator
    {
        public BaseLogicalOperator(int line, int col, string text) : base(line, col, text)
        {
        }
        public abstract string AssemblyShortCircuitCode(BaseTypeNode arg1, BaseTypeNode arg2);
    }

    public class LogicalSumLogicalOperator : BaseLogicalOperator
    {
        public LogicalSumLogicalOperator(LexicalInfo lexicalInfo) 
            : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }

        public override string Assembly()
        {
            return "or\n";
        }


        public override string AssemblyShortCircuitCode(BaseTypeNode arg1, BaseTypeNode arg2)
        {
            string code = string.Empty;
            string trueLabel = LabelProvider.GetNewLabel();
            string endLabel = LabelProvider.GetNewLabel();
            code += arg1.Assembly();
            code += $"brtrue {trueLabel}\n";
            code += arg2.Assembly();
            code += $"br {endLabel}\n";
            code += $"{trueLabel}: ldc.i4 1\n";
            code += $"{endLabel}:\n";
            return code;
        }

        public override TypeEnum GetResultType(BaseTypeNode arg1, BaseTypeNode arg2)
        {
            var arg1Type = arg1.GetTypeEnum();
            var arg2Type = arg2.GetTypeEnum();
            if (arg1Type == TypeEnum.Invalid || arg2Type == TypeEnum.Invalid) return TypeEnum.Invalid;
            if (arg1Type == TypeEnum.Bool && arg2Type == TypeEnum.Bool)
            {
                return TypeEnum.Bool;
            }
            ErrorMessages.AddBinaryTypeMismatch("||", "(bool) (bool)", $"({arg1Type.ToString()}) ({arg2Type.ToString()})", Line);
            return TypeEnum.Invalid;
        }
    }

    public class LogicalMulLogicalOperator : BaseLogicalOperator
    {
        public LogicalMulLogicalOperator(LexicalInfo lexicalInfo)
            : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }

        public override string Assembly()
        {
            return "and\n";
        }

        public override string AssemblyShortCircuitCode(BaseTypeNode arg1, BaseTypeNode arg2)
        {
            string code = string.Empty;
            string falseLabel = LabelProvider.GetNewLabel();
            string endLabel = LabelProvider.GetNewLabel();
            code += arg1.Assembly();
            code += $"brfalse {falseLabel}\n";
            code += arg2.Assembly();
            code += $"br {endLabel}\n";
            code += $"{falseLabel}: ldc.i4 0\n";
            code += $"{endLabel}:\n";
            return code;
        }

        public override TypeEnum GetResultType(BaseTypeNode arg1, BaseTypeNode arg2)
        {
            var arg1Type = arg1.GetTypeEnum();
            var arg2Type = arg2.GetTypeEnum();
            if (arg1Type == TypeEnum.Invalid || arg2Type == TypeEnum.Invalid) return TypeEnum.Invalid;
            if (arg1Type == TypeEnum.Bool && arg2Type == TypeEnum.Bool)
            {
                return TypeEnum.Bool;
            }
            ErrorMessages.AddBinaryTypeMismatch("&&", "(bool) (bool)", $"({arg1Type.ToString()}) ({arg2Type.ToString()})", Line);
            return TypeEnum.Invalid;
        }
    }
}
