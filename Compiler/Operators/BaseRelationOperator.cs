﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseRelationOperator : TwoArgumentOperator
    {
        public BaseRelationOperator(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class GreaterThanRelationOperator : BaseRelationOperator
    {
        public GreaterThanRelationOperator(LexicalInfo lexicalInfo)
            : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }

        public override string Assembly()

        {
            return "cgt\n";
        }

        public override TypeEnum GetResultType(BaseTypeNode arg1, BaseTypeNode arg2)
        {
            var arg1Type = arg1.GetTypeEnum();
            var arg2Type = arg2.GetTypeEnum();
            if (arg1Type == TypeEnum.Invalid || arg2Type == TypeEnum.Invalid) return TypeEnum.Invalid;
            if (
                (arg1Type == TypeEnum.Int && arg2Type == TypeEnum.Int)       ||
                (arg1Type == TypeEnum.Double && arg2Type == TypeEnum.Double)
               )
            {
                return TypeEnum.Bool;
            }
            if (arg1Type == TypeEnum.Int && arg2Type == TypeEnum.Double)
            {
                arg1CastCommand = "conv.r8\n";
                return TypeEnum.Bool;
            }
            if (arg1Type == TypeEnum.Double && arg2Type == TypeEnum.Int)
            {
                arg2CastCommand = "conv.r8\n";
                return TypeEnum.Bool;
            }
            ErrorMessages.AddBinaryTypeMismatch(">", "(int|double) (int|double)", $"({arg1Type.ToString()}) ({arg2Type.ToString()})", Line);
            return TypeEnum.Invalid;
        }
    }
    public class LessThanRelationOperator : BaseRelationOperator
    {
        public LessThanRelationOperator(LexicalInfo lexicalInfo)
            : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }
        public override string Assembly()
        {
            return "clt\n";
        }
        public override TypeEnum GetResultType(BaseTypeNode arg1, BaseTypeNode arg2)
        {
            var arg1Type = arg1.GetTypeEnum();
            var arg2Type = arg2.GetTypeEnum();
            if (arg1Type == TypeEnum.Invalid || arg2Type == TypeEnum.Invalid) return TypeEnum.Invalid;
            if (
                (arg1Type == TypeEnum.Int && arg2Type == TypeEnum.Int) ||
                (arg1Type == TypeEnum.Double && arg2Type == TypeEnum.Double)
               )
            {
                return TypeEnum.Bool;
            }
            if (arg1Type == TypeEnum.Int && arg2Type == TypeEnum.Double)
            {
                arg1CastCommand = "conv.r8\n";
                return TypeEnum.Bool;
            }
            if (arg1Type == TypeEnum.Double && arg2Type == TypeEnum.Int)
            {
                arg2CastCommand = "conv.r8\n";
                return TypeEnum.Bool;
            }
            ErrorMessages.AddBinaryTypeMismatch("<", "(int|double) (int|double)", $"({arg1Type.ToString()}) ({arg2Type.ToString()})", Line);
            return TypeEnum.Invalid;
        }
    }
    public class GreaterThanEqualRelationOperator : BaseRelationOperator
    {
        public GreaterThanEqualRelationOperator(LexicalInfo lexicalInfo)
            : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }
        public override string Assembly()
        {
            return "clt\nldc.i4.0\nceq\n";
        }
        public override TypeEnum GetResultType(BaseTypeNode arg1, BaseTypeNode arg2)
        {
            var arg1Type = arg1.GetTypeEnum();
            var arg2Type = arg2.GetTypeEnum();
            if (arg1Type == TypeEnum.Invalid || arg2Type == TypeEnum.Invalid) return TypeEnum.Invalid;
            if (
                (arg1Type == TypeEnum.Int && arg2Type == TypeEnum.Int) ||
                (arg1Type == TypeEnum.Double && arg2Type == TypeEnum.Double)
               )
            {
                return TypeEnum.Bool;
            }
            if (arg1Type == TypeEnum.Int && arg2Type == TypeEnum.Double)
            {
                arg1CastCommand = "conv.r8\n";
                return TypeEnum.Bool;
            }
            if (arg1Type == TypeEnum.Double && arg2Type == TypeEnum.Int)
            {
                arg2CastCommand = "conv.r8\n";
                return TypeEnum.Bool;
            }
            ErrorMessages.AddBinaryTypeMismatch(">=", "(int|double) (int|double)", $"({arg1Type.ToString()}) ({arg2Type.ToString()})", Line);
            return TypeEnum.Invalid;
        }
    }
    public class LessThanEqualRelationOperator : BaseRelationOperator
    {
        public LessThanEqualRelationOperator(LexicalInfo lexicalInfo)
            : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }
        public override string Assembly()
        {
            return "cgt\nldc.i4.0\nceq\n";
        }
        public override TypeEnum GetResultType(BaseTypeNode arg1, BaseTypeNode arg2)
        {
            var arg1Type = arg1.GetTypeEnum();
            var arg2Type = arg2.GetTypeEnum();
            if (arg1Type == TypeEnum.Invalid || arg2Type == TypeEnum.Invalid) return TypeEnum.Invalid;
            if (
                (arg1Type == TypeEnum.Int && arg2Type == TypeEnum.Int) ||
                (arg1Type == TypeEnum.Double && arg2Type == TypeEnum.Double)
               )
            {
                return TypeEnum.Bool;
            }
            if (arg1Type == TypeEnum.Int && arg2Type == TypeEnum.Double)
            {
                arg1CastCommand = "conv.r8\n";
                return TypeEnum.Bool;
            }
            if (arg1Type == TypeEnum.Double && arg2Type == TypeEnum.Int)
            {
                arg2CastCommand = "conv.r8\n";
                return TypeEnum.Bool;
            }
            ErrorMessages.AddBinaryTypeMismatch("<=", "(int|double) (int|double)", $"({arg1Type.ToString()}) ({arg2Type.ToString()})", Line);
            return TypeEnum.Invalid;
        }
    }
    public class NotEqualRelationOperator : BaseRelationOperator
    {
        public NotEqualRelationOperator(LexicalInfo lexicalInfo)
            : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }
        public override string Assembly()
        {
            return "ceq\nldc.i4.0\nceq\n";
        }
        public override TypeEnum GetResultType(BaseTypeNode arg1, BaseTypeNode arg2)
        {
            var arg1Type = arg1.GetTypeEnum();
            var arg2Type = arg2.GetTypeEnum();
            if (arg1Type == TypeEnum.Invalid || arg2Type == TypeEnum.Invalid) return TypeEnum.Invalid;
            if (
                (arg1Type == TypeEnum.Int && arg2Type == TypeEnum.Int) ||
                (arg1Type == TypeEnum.Double && arg2Type == TypeEnum.Double) ||
                (arg1Type == TypeEnum.Bool && arg2Type == TypeEnum.Bool)
               )
            {
                return TypeEnum.Bool;
            }
            if (arg1Type == TypeEnum.Int && arg2Type == TypeEnum.Double)
            {
                arg1CastCommand = "conv.r8\n";
                return TypeEnum.Bool;
            }
            if (arg1Type == TypeEnum.Double && arg2Type == TypeEnum.Int)
            {
                arg2CastCommand = "conv.r8\n";
                return TypeEnum.Bool;
            }
            ErrorMessages.AddBinaryTypeMismatch("!=", "(int|double|bool) (int|double|bool)", $"({arg1Type.ToString()}) ({arg2Type.ToString()})", Line);
            return TypeEnum.Invalid;
        }
    }
    public class EqualRelationOperator : BaseRelationOperator
    {
        public EqualRelationOperator(LexicalInfo lexicalInfo)
            : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }
        public override string Assembly()
        {
            return "ceq\n";
        }
        public override TypeEnum GetResultType(BaseTypeNode arg1, BaseTypeNode arg2)
        {
            var arg1Type = arg1.GetTypeEnum();
            var arg2Type = arg2.GetTypeEnum();
            if (arg1Type == TypeEnum.Invalid || arg2Type == TypeEnum.Invalid) return TypeEnum.Invalid;
            if (
                (arg1Type == TypeEnum.Int && arg2Type == TypeEnum.Int) ||
                (arg1Type == TypeEnum.Double && arg2Type == TypeEnum.Double) ||
                (arg1Type == TypeEnum.Bool && arg2Type == TypeEnum.Bool)
               )
            {
                return TypeEnum.Bool;
            }
            if (arg1Type == TypeEnum.Int && arg2Type == TypeEnum.Double)
            {
                arg1CastCommand = "conv.r8\n";
                return TypeEnum.Bool;
            }
            if (arg1Type == TypeEnum.Double && arg2Type == TypeEnum.Int)
            {
                arg2CastCommand = "conv.r8\n";
                return TypeEnum.Bool;
            }
            ErrorMessages.AddBinaryTypeMismatch("==", "(int|double|bool) (int|double|bool)", $"({arg1Type.ToString()}) ({arg2Type.ToString()})", Line);
            return TypeEnum.Invalid;
        }
    }
}
