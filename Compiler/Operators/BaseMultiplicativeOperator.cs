﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseMultiplicativeOperator : TwoArgumentOperator
    {
        public BaseMultiplicativeOperator(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class MultiplyMultiplicativeOperator : BaseMultiplicativeOperator
    {
        public MultiplyMultiplicativeOperator(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }

        public override string Assembly()
        {
            return "mul\n";
        }

        public override TypeEnum GetResultType(BaseTypeNode arg1, BaseTypeNode arg2)
        {
            var arg1Type = arg1.GetTypeEnum();
            var arg2Type = arg2.GetTypeEnum();
            if (arg1Type == TypeEnum.Invalid || arg2Type == TypeEnum.Invalid) return TypeEnum.Invalid;
            if (arg1Type == TypeEnum.Int && arg2Type == TypeEnum.Int) return TypeEnum.Int;
            if (arg1Type == TypeEnum.Int && arg2Type == TypeEnum.Double)
            {
                arg1CastCommand = "conv.r8\n";
                return TypeEnum.Double;
            }
            if (arg1Type == TypeEnum.Double && arg2Type == TypeEnum.Int)
            {
                arg2CastCommand = "conv.r8\n";
                return TypeEnum.Double;
            }
            if (arg1Type == TypeEnum.Double && arg2Type == TypeEnum.Double)
            {
                return TypeEnum.Double;
            }
            ErrorMessages.AddBinaryTypeMismatch("*", "(int|double) (int|double)", $"({arg1Type.ToString()}) ({arg2Type.ToString()})", Line);
            return TypeEnum.Invalid;
        }
    }
    public class DivideMultiplicativeOperator : BaseMultiplicativeOperator
    {
        public DivideMultiplicativeOperator(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }

        public override string Assembly()
        {
            return "div\n";
        }

        public override TypeEnum GetResultType(BaseTypeNode arg1, BaseTypeNode arg2)
        {
            var arg1Type = arg1.GetTypeEnum();
            var arg2Type = arg2.GetTypeEnum();
            if (arg1Type == TypeEnum.Invalid || arg2Type == TypeEnum.Invalid) return TypeEnum.Invalid;
            if (arg1Type == TypeEnum.Int && arg2Type == TypeEnum.Int) return TypeEnum.Int;
            if (arg1Type == TypeEnum.Int && arg2Type == TypeEnum.Double)
            {
                arg1CastCommand = "conv.r8\n";
                return TypeEnum.Double;
            }
            if (arg1Type == TypeEnum.Double && arg2Type == TypeEnum.Int)
            {
                arg2CastCommand = "conv.r8\n";
                return TypeEnum.Double;
            }
            if (arg1Type == TypeEnum.Double && arg2Type == TypeEnum.Double)
            {
                return TypeEnum.Double;
            }
            ErrorMessages.AddBinaryTypeMismatch("/", "(int|double) (int|double)", $"({arg1Type.ToString()}) ({arg2Type.ToString()})", Line);
            return TypeEnum.Invalid;
        }
    }
}
