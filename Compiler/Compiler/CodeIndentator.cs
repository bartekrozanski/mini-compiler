﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public static class CodeIndentator
    {

        private static string GetIndentString(int n)
        {
            var sb = new StringBuilder();
            for(int i = 0; i < n; ++i)
            {
                sb.Append("  ");
            }
            return sb.ToString();
        }

        public static string IndentCode(string code)
        {
            var sb = new StringBuilder(code);
            int indentLevel = 0;
            for(int i = 0; i < sb.Length; ++i)
            {
                if (sb[i] == '{' || sb[i] == '(') indentLevel++;
                else if (sb[i] == '}' || sb[i] == ')') indentLevel--;
                else if (sb[i] == '\n') {
                    sb.Insert(i+1, GetIndentString(indentLevel));
                };
            }
            if (indentLevel != 0) throw new ArgumentException("Mismatched number of opening and closing brackets/parentheses");
            return sb.ToString();
        }

    }
}
