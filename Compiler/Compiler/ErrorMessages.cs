﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public static class ErrorMessages
    {
        public static List<string> Errors { get; } = new List<string>();

        public static void Append(string msg)
        {
            Errors.Add(msg);
        }
        public static void AddDuplicatedDeclaration(string varId, int line)
        {
            Errors.Add($"Variable already declared - '{varId}' duplicated at line {line}.");
        }
        public static void AddUndeclaredVariable(string varId, int line)
        { 
            Errors.Add($"Undeclared variable '{varId}' at line {line}.");
        }
        public static void AddBinaryTypeMismatch(string opSign, string expected, string got, int line)
        {
            Errors.Add($"Operator '{opSign}' cannot be applied to operands of type {got}. Expected type [{expected}] at line {line}.");
        }
        public static void AddUnaryTypeMismatch(string opSign, string expected, string gotType, int line)
        {
            Errors.Add($"Operator '{opSign}' cannot be applied to operand of type [{gotType}]. Expected type [{expected}] at line {line}.");
        }
        public static void AddInvalidExpressionType(string instruction, string expectedType, string gotType, int line)
        {
            Errors.Add($"Instruction '{instruction}' cannot be used with expression of type [{gotType}]. Expected type [{expectedType}] at line {line}.");
        }
        public static void AddSyntaxErrorDanglingSemicolon(int line)
        {
            Errors.Add($"Syntax error at line {line}. Probably dangling semicolon.");
        }
        public static void AddSyntaxErrorMissingSemicolon(int line)
        {
            Errors.Add($"Syntax error at line {line}. Probably missing semicolon.");
        }

        public static void AddSyntaxErrorMissingSemicolonApprox(int line)
        {
            Errors.Add($"Syntax error before line {line}. Did you forgot semicolon?");
        }
        public static void AddSyntaxErrorEOF(int line)
        {
            Errors.Add($"Syntax error at line {line} - probably unexpected EOF.");
        }

        public static void AddSyntaxErrorDeclarationNotAtTop(int line)
        {
            Errors.Add($"Syntax error at line {line} - declarations can appear only at the top of the program block.");
        }

        public static void PrintErrors()
        {
            foreach (var error in ErrorMessages.Errors)
            {
                Console.WriteLine(error);
            }
        }
    }
}
