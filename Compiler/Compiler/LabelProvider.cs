﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public static class LabelProvider
    {
        private static int _labelCount = 0;
        public static readonly string LastLine = "END_0";
        public static string GetNewLabel()
        {
            return $"L_{_labelCount++}";
        }
    }
}
