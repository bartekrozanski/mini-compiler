﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public class VariableInfo
    {

        public int IntId { get; }
        public BaseValueType ValueType {get;}
        public VariableInfo(int IntId, BaseValueType ValueType)
        {
            this.IntId = IntId;
            this.ValueType = ValueType;
        }
    }
    public static class Context
    {

        public static IntValueType IntValueTypeNode { get; } = new IntValueType(-100, -100, "CONTEXTINTNODE");
        public static DoubleValueType DoubleValueTypeNode { get; } = new DoubleValueType(-100, -100, "CONTEXTDOUBLENODE");
        public static BoolValueType BoolValueTypeNode { get; } = new BoolValueType(-100, -100, "CONTEXTBOOLNODE");

        private static Dictionary<string, VariableInfo> IdMapping = 
            new Dictionary<string, VariableInfo>();
        public static int Count => IdMapping.Count;

        public static bool Exists(string idString)
        {
            return IdMapping.ContainsKey(idString);
        }
        public static int GetIntId(string idString)
        {
            return IdMapping[idString].IntId;
        }

        public static BaseValueType GetValueType(string idString)
        {
            return IdMapping[idString].ValueType;
        }

        public static void AddVariable(
            string stringId,
            int intId,
            string type
            )
        {
            if(IntValueTypeNode.Type == type)
            {
                IdMapping.Add(stringId, new VariableInfo(intId, IntValueTypeNode));
            }
            else if(DoubleValueTypeNode.Type == type)
            {
                IdMapping.Add(stringId, new VariableInfo(intId, DoubleValueTypeNode));
            }
            else if(BoolValueTypeNode.Type == type)
            {
                IdMapping.Add(stringId, new VariableInfo(intId, BoolValueTypeNode));
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }
        }

        public static string GenerateDefaultInitialization()
        {
            string code = string.Empty;
            foreach (var variable in IdMapping)
            {
                BaseValueType type = variable.Value.ValueType;
                code += $"{type.LoadConst} {type.DefaultValue}\n";
                code += $"stloc {variable.Value.IntId}\n";
            }
            return code;
        }










    }
}
