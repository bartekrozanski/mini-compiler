﻿using GardensPoint;
using QUT.Gppg;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MiniCompiler
{
    class Program
    {
        static int Main(string[] args)
        {
            if(args.Length != 1)
            {
                Console.WriteLine("Usage:");
                Console.WriteLine($"{System.AppDomain.CurrentDomain.FriendlyName} [file.mini]");
                return 1;
            }
            FileStream sourceCode = new FileStream(args[0], FileMode.Open);

            Scanner scanner = new Scanner(sourceCode);
            Parser parser = new Parser(scanner, new MainStart());

            bool success = parser.Parse();
            MainStart mainNode = parser.MainNode;
            if(!success)
            {
                Console.WriteLine("Parse error");
                return 1;
            }
            if (ErrorMessages.Errors.Count != 0)
            {
                ErrorMessages.PrintErrors();
                return 1;
            }
            string code;
            code = mainNode.Assembly();
            if (ErrorMessages.Errors.Count != 0)
            {
                ErrorMessages.PrintErrors();
                return 1;
            }
            else
            {
                code = CodeIndentator.IndentCode(code);
                string outputPath = $"{args[0]}.il";
                File.WriteAllText(outputPath, code);
                Console.WriteLine($"Assembled code written to {outputPath}");
                return 0;
            }
        }

    }
}
