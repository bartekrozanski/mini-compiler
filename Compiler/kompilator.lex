﻿%using MiniCompiler;
%namespace GardensPoint

ValDouble	(0|([1-9][0-9]*))+\.([0-9])+
ValInt		0|[1-9]([0-9])*
ValBool		true|false
String		\"([^\\\"\n]|\\.)*\"
Id			[a-zA-Z][a-zA-Z0-9]*
CastInt		\([ \t]*int[ \t]*\)
CastDouble	\([ \t]*double[ \t]*\)
Comment		\/\/([^\n]|\\.)*

%%
"program"		{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.PROGRAM;			}
"if"			{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.IF;				}
"else"			{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.ELSE;				}
"while"			{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.WHILE;			}
"read"			{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.READ;				}
"write"			{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.WRITE;			}
"return"		{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.RETURN;			}
"int"			{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.INT;				}
"double"		{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.DOUBLE;			}
"bool"			{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.BOOL;				}
"="				{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.ASSIGNMENT;		}
"||"			{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.LOGICSUM;			}
"&&"			{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.LOGICMUL;			}
"|"				{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.BITSUM;			}
"&"				{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.BITMUL;			}
"=="			{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.EQ;				}
"!="			{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.NOTEQ;			}
">"				{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.GT;				}
">="			{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.GTE;				}
"+"				{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.ADD;				}			
"<"				{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.LT;				}
"<="			{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.LTE;				}
"*"				{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.MUL;				}
"/"				{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.DIV;				}
"-"				{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.MINUS;			}
"!"				{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.UNLOGICNOT;		}
"~"				{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.UNBITNOT;			}
{CastDouble}	{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.UNDOUBLECAST;		}		
{CastInt}		{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.UNINTCAST;		}
"("				{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.OPARENT;			}
")"				{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.CPARENT;			}
"{"				{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.OBRACKET;			}
"}"				{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.CBRACKET;			}
";"				{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.SEMICOLON;		}
" "				{																						}
"\t"			{																						}
"\n"			{																						}
"\r"			{																						}
{Comment}		{																						}
{ValDouble}		{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.VALDOUBLE;		}
{ValInt}		{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.VALINT;			}
{ValBool}		{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.VALBOOL;			}
{String}		{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.STRING;			}
{Id}			{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.ID;				}
<<EOF>>			{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.EOF;				}
.				{ yylval = new LexicalInfo(yyline, yycol, yytext); return (int)Tokens.error;			}
%%


public override void yyerror(string format, params object[] args)
{
	Console.WriteLine("Scanner builtin error message: " + format);
}
