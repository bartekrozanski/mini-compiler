﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public class LexicalInfo
    {
        public int Line { get; set; }
        public int Col { get; set; }
        public string Text { get; set; }

        public LexicalInfo(int line, int col, string text)
        {
            Line = line;
            Col = col;
            Text = text;
        }
    }

}
