﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class OneArgumentOperator : BaseNode
    {
        public OneArgumentOperator(int line, int col, string text) : base(line, col, text)
        {
        }
        public abstract TypeEnum GetResultType(BaseTypeNode arg);
    }
}
