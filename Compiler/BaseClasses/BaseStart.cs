﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseStart : BaseNode
    {
        public BaseStart(int line, int col, string text) : base(line, col, text)
        {
        }

        public override string Assembly()
        {
            return base.Assembly();
        }
    }

    public class MainStart : BaseStart
    {
        BaseProgramKeyword ProgramKeyword { get; set; }
        BaseOBracketKeyword OBracketKeyword { get; set; }
        BaseDeclarationSequence DeclarationSequence { get; set; }
        BaseInstructionSequence InstructionSequence { get; set; }
        BaseCBracketKeyword CBracketKeyword { get; set; }
        BaseEof Eof { get; set; }

        public MainStart(
            BaseProgramKeyword ProgramKeyword,
            BaseOBracketKeyword OBracketKeyword,
            BaseDeclarationSequence DeclarationSequence,
            BaseInstructionSequence InstructionSequence,
            BaseCBracketKeyword CBracketKeyword,
            BaseEof Eof
            ) : base(ProgramKeyword.Line, ProgramKeyword.Col, ProgramKeyword.Text)
        {
            this.ProgramKeyword = ProgramKeyword;
            this.OBracketKeyword = OBracketKeyword;
            this.DeclarationSequence = DeclarationSequence;
            this.InstructionSequence = InstructionSequence;
            this.CBracketKeyword = CBracketKeyword;
            this.Eof = Eof;
        }
        public MainStart() : base(-1, -1, "")
        {
        }

        public override string Assembly()
        {
            string code = string.Empty;
            code += ".assembly MiniCompiler {}\n";
            code += ".assembly extern mscorlib {}\n";
            code += ".method static void Main()\n";
            code += "{\n";
            code += ".entrypoint\n";
            //code += ProgramKeyword.Assembly();
            //code += OBracketKeyword.Assembly();
            code += ".locals init (\n";
            code += DeclarationSequence.Assembly();
            code += "\n)\n";
            code += Context.GenerateDefaultInitialization();
            code += InstructionSequence.Assembly();
            //code += CBracketKeyword.Assembly();
            code += LabelProvider.LastLine + ": ret\n}\n";
            return code;
        }
    }
}
