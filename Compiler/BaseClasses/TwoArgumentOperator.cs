﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class TwoArgumentOperator : BaseNode
    {
        public TwoArgumentOperator(int line, int col, string text) : base(line, col, text)
        {
        }
        public abstract TypeEnum GetResultType(BaseTypeNode arg1, BaseTypeNode arg2);
        public string arg1CastCommand = "";
        public string arg2CastCommand = "";
    }
}
