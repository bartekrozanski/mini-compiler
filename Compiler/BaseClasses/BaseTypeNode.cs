﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public enum TypeEnum {
        Int,
        Double,
        Bool,
        None,
        Invalid
    }
    public abstract class BaseTypeNode : BaseNode
    {

        protected TypeEnum typeEnum = TypeEnum.None; 
        public BaseTypeNode(int line, int col, string text) : base(line, col, text)
        {
        }

        protected abstract void UpdateTypeEnum();
        public TypeEnum GetTypeEnum()
        {
            if (typeEnum == TypeEnum.None) UpdateTypeEnum();
            return typeEnum;
        }
        public abstract override string Assembly();
    }
}
