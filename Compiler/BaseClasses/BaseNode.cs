﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseNode : LexicalInfo
    {
        public virtual string Assembly()
        {
            return string.Empty;
        }
        public BaseNode(int line, int col, string text) : base(line, col, text)
        {
        }
    }
}
