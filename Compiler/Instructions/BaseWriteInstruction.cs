﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseWriteInstruction : BaseNode
    {
        public BaseWriteInstruction(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class WriteStringWriteInstruction : BaseWriteInstruction
    {
        BaseWriteKeyword WriteKeyword { get; set; }
        BaseStringKeyword StringKeyword { get; set; }
        BaseSemicolonKeyword SemicolonKeyword { get; set; }
        public WriteStringWriteInstruction(
            BaseWriteKeyword WriteKeyword,
            BaseStringKeyword StringKeyword,
            BaseSemicolonKeyword SemicolonKeyword
            ) : base(StringKeyword.Line, StringKeyword.Col, StringKeyword.Text)
        {
            this.WriteKeyword = WriteKeyword;
            this.StringKeyword = StringKeyword;
            this.SemicolonKeyword = SemicolonKeyword;
        }

        public override string Assembly()
        {
            string code = StringKeyword.Assembly();
            code += "call void [mscorlib]System.Console::Write(string)\n";
            return code;
        }
    }

    public class WriteIdWriteInstruction : BaseWriteInstruction
    {
        BaseWriteKeyword WriteKeyword { get; set; }
        BaseAssignmentExpression AssignmentExpression { get; set; }
        BaseSemicolonKeyword SemicolonKeyword { get; set; }
        public WriteIdWriteInstruction(
            BaseWriteKeyword WriteKeyword,
            BaseAssignmentExpression AssignmentExpression,
            BaseSemicolonKeyword SemicolonKeyword
            ) : base(AssignmentExpression.Line, AssignmentExpression.Col, AssignmentExpression.Text)
        {
            this.WriteKeyword = WriteKeyword;
            this.AssignmentExpression = AssignmentExpression;
            this.SemicolonKeyword = SemicolonKeyword;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            switch(AssignmentExpression.GetTypeEnum())
            {
                case TypeEnum.Bool:
                    code += AssignmentExpression.Assembly();
                    code += $"call void [System.Console]System.Console::Write(bool)\n";
                    break;
                case TypeEnum.Double:
                    code += $"call class [mscorlib]System.Globalization.CultureInfo [mscorlib]System.Globalization.CultureInfo::get_InvariantCulture()\n";
                    code += "ldstr \"{0:0.000000}\"\n";
                    code += AssignmentExpression.Assembly();
                    code += "box [mscorlib]System.Double\n";
                    code += $"call string [mscorlib]System.String::Format(class [mscorlib]System.IFormatProvider, string, object)\n";
                    code += $"call void [mscorlib]System.Console::Write(string)\n";
                    break;
                case TypeEnum.Int:
                    code += AssignmentExpression.Assembly();
                    code += $"call void [mscorlib]System.Console::Write(int32)\n";
                    break;
                default:
                    throw new NotImplementedException();
            }
            return code;
        }
    }
}
