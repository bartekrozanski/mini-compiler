﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseExpressionInstruction : BaseNode
    {
        public BaseExpressionInstruction(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class ExpressionExpressionInstruction : BaseExpressionInstruction
    {
        BaseAssignmentExpression AssignmentExpression { get; set; }
        BaseSemicolonKeyword SemicolonKeyword { get; set; }
        public ExpressionExpressionInstruction(
            BaseAssignmentExpression AssignmentExpression,
            BaseSemicolonKeyword SemicolonKeyword
            ) : base(AssignmentExpression.Line, AssignmentExpression.Col, AssignmentExpression.Text)
        {
            this.AssignmentExpression = AssignmentExpression;
            this.SemicolonKeyword = SemicolonKeyword;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            code += AssignmentExpression.Assembly();
            code += "pop\n";
            return code;
        }
    }
    public class SyntaxErrorMissingSemicolonExpressionInstruction : BaseExpressionInstruction
    {
        public SyntaxErrorMissingSemicolonExpressionInstruction(LexicalInfo lexicalInfo) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
            ErrorMessages.AddSyntaxErrorMissingSemicolonApprox(Line);
        }

        public override string Assembly()
        {
            throw new NotImplementedException();
        }
    }
}
