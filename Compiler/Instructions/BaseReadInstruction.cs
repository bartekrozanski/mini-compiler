﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseReadInstruction : BaseNode
    {
        public BaseReadInstruction(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class ReadReadInstruction : BaseReadInstruction
    {
        BaseReadKeyword ReadKeyword { get; set; }
        BaseIdKeyword IdKeyword { get; set; }
        BaseSemicolonKeyword SemicolonKeyword { get; set; }

        public ReadReadInstruction(
            BaseReadKeyword ReadKeyword,
            BaseIdKeyword IdKeyword,
            BaseSemicolonKeyword SemicolonKeyword
            ) : base(IdKeyword.Line, IdKeyword.Col, IdKeyword.Text)
        {
            this.ReadKeyword = ReadKeyword;
            this.IdKeyword = IdKeyword;
            this.SemicolonKeyword = SemicolonKeyword;
        }

        public override string Assembly()
        {
            BaseValueType type = IdKeyword.ValueType;
            string code = string.Empty;
            code += ReadKeyword.Assembly();
            if(type.GetTypeEnum() == TypeEnum.Bool)
            {
                code += $"call {type.Type} [mscorlib]System.{type.TypeModuleName}::Parse(string)\n";
            }
            else
            {
                code += $"call class [mscorlib]System.Globalization.CultureInfo [mscorlib]System.Globalization.CultureInfo::get_InvariantCulture()\n";
                code += $"call {type.Type} [mscorlib]System.{type.TypeModuleName}::Parse(string, class [mscorlib] System.IFormatProvider)\n";
            }
            code += $"stloc {IdKeyword.IntId}\n";
            return code;
        }
    }
}
