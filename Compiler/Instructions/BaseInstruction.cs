﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseInstruction : BaseNode
    {
        public BaseInstruction(int line, int col, string text) : base(line, col, text)
        {
        }
    }


    public class ExpressionInstruction : BaseInstruction
    {
        BaseExpressionInstruction ExpressionInstruction_ { get; set; }
        public ExpressionInstruction(
            BaseExpressionInstruction ExpressionInstruction_
            ) : base(ExpressionInstruction_.Line, ExpressionInstruction_.Col, ExpressionInstruction_.Text)
        {
            this.ExpressionInstruction_ = ExpressionInstruction_;
        }

        public override string Assembly()
        {
            return ExpressionInstruction_.Assembly();
        }
    }

    public class IfInstruction : BaseInstruction
    {
        BaseIfInstruction IfInstruction_ { get; set; 
        }
        public IfInstruction(
            BaseIfInstruction IfInstruction_
            ) : base(IfInstruction_.Line, IfInstruction_.Col, IfInstruction_.Text)
        {
            this.IfInstruction_ = IfInstruction_;
        }

        public override string Assembly()
        {
            return IfInstruction_.Assembly();
        }
    }

    public class WhileInstruction : BaseInstruction
    {
        BaseWhileInstruction WhileInstruction_ { get; set; }
        public WhileInstruction(
            BaseWhileInstruction WhileInstruction_
            ) : base(WhileInstruction_.Line, WhileInstruction_.Col, WhileInstruction_.Text)
        {
            this.WhileInstruction_ = WhileInstruction_;
        }
        public override string Assembly()
        {
            return WhileInstruction_.Assembly();
        }
    }

    public class ReadInstruction : BaseInstruction
    {
        BaseReadInstruction ReadInstruction_ { get; set; }
        public ReadInstruction(
            BaseReadInstruction ReadInstruction_
            ) : base(ReadInstruction_.Line, ReadInstruction_.Col, ReadInstruction_.Text)
        {
            this.ReadInstruction_ = ReadInstruction_;
        }

        public override string Assembly()
        {
            return ReadInstruction_.Assembly();
        }
    }


    public class WriteInstruction : BaseInstruction
    {
        BaseWriteInstruction WriteInstruction_ {get;set;}
        public WriteInstruction(
            BaseWriteInstruction WriteInstruction_
            ) : base(WriteInstruction_.Line, WriteInstruction_.Col, WriteInstruction_.Text)
        {
            this.WriteInstruction_ = WriteInstruction_;
        }

        public override string Assembly()
        {
            return WriteInstruction_.Assembly();
        }
    }

    public class ReturnInstruction : BaseInstruction
    {
        BaseReturnInstruction ReturnInstruction_ { get; set; }
        public ReturnInstruction(
            BaseReturnInstruction ReturnInstruction_
            ) : base(ReturnInstruction_.Line, ReturnInstruction_.Col, ReturnInstruction_.Text)
        {
            this.ReturnInstruction_ = ReturnInstruction_;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            code += ReturnInstruction_.Assembly();
            return code;
        }
    }

    public class BlockInstruction : BaseInstruction
    {
        BaseBlockInstruction BlockInstruction_ { get; set; } 
        public BlockInstruction(
            BaseBlockInstruction BlockInstruction_
            ) : base(BlockInstruction_.Line, BlockInstruction_.Col, BlockInstruction_.Text)
        {
            this.BlockInstruction_ = BlockInstruction_;
        }

        public override string Assembly()
        {
            return BlockInstruction_.Assembly();
        }
    }
    public class SyntaxErrorDanglingSemicolonInstruction : BaseInstruction
    {
        public SyntaxErrorDanglingSemicolonInstruction(
            LexicalInfo SemicolonKeyword
            ) : base(SemicolonKeyword.Line, SemicolonKeyword.Col, SemicolonKeyword.Text)
        {
            ErrorMessages.AddSyntaxErrorDanglingSemicolon(Line);
        }

        public override string Assembly()
        {
            throw new InvalidOperationException();
        }
    }
    public class SyntaxErrorMissingSemicolonInstruction : BaseInstruction
    {
        public SyntaxErrorMissingSemicolonInstruction(
            LexicalInfo SemicolonKeyword
            ) : base(SemicolonKeyword.Line, SemicolonKeyword.Col, SemicolonKeyword.Text)
        {
            ErrorMessages.AddSyntaxErrorMissingSemicolonApprox(Line);
        }

        public override string Assembly()
        {
            throw new InvalidOperationException();
        }
    }
    public class SyntaxErrorDeclarationAsInstruction : BaseInstruction
    {
        public SyntaxErrorDeclarationAsInstruction(
            LexicalInfo SemicolonKeyword
            ) : base(SemicolonKeyword.Line, SemicolonKeyword.Col, SemicolonKeyword.Text)
        {
            ErrorMessages.AddSyntaxErrorDeclarationNotAtTop(Line);
        }

        public override string Assembly()
        {
            throw new InvalidOperationException();
        }
    }
    
}