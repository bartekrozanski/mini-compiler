﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseWhileInstruction : BaseNode
    {
        public BaseWhileInstruction(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class WhileWhileInstruction : BaseWhileInstruction
    {
        BaseWhileKeyword WhileKeyword { get; set; }
        BaseOParentKeyword OParentKeyword { get; set; }
        BaseAssignmentExpression AssignmentExpression { get; set; }
        BaseCParentKeyword CParentKeyword { get; set; }
        BaseInstruction Instruction { get; set; }

        public WhileWhileInstruction(
            BaseWhileKeyword WhileKeyword,
            BaseOParentKeyword OParentKeyword,
            BaseAssignmentExpression AssignmentExpression,
            BaseCParentKeyword CParentKeyword,
            BaseInstruction Instruction
            ) : base(AssignmentExpression.Line, AssignmentExpression.Col, AssignmentExpression.Text)
        {
            this.WhileKeyword = WhileKeyword;
            this.OParentKeyword = OParentKeyword;
            this.AssignmentExpression = AssignmentExpression;
            this.CParentKeyword = CParentKeyword;
            this.Instruction = Instruction;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            string conditionLabel = LabelProvider.GetNewLabel();
            string loopHeadLabel = LabelProvider.GetNewLabel();
            code += $"br {conditionLabel}\n";
            code += $"{loopHeadLabel}: ";
            code += Instruction.Assembly();
            code += $"{conditionLabel}: ";
            code += AssignmentExpression.Assembly();
            code += $"brtrue {loopHeadLabel}\n";
            if(AssignmentExpression.GetTypeEnum() != TypeEnum.Bool)
            {
                ErrorMessages.AddInvalidExpressionType("while", "(bool)", AssignmentExpression.GetTypeEnum().ToString(), WhileKeyword.Line);
            }
            return code;
        }
    }
}
