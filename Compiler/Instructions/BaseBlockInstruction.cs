﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseBlockInstruction : BaseNode
    {
        public BaseBlockInstruction(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class BlockBlockInstruction : BaseBlockInstruction
    {
        BaseOBracketKeyword OBracketKeyword { get; set; }
        BaseInstructionSequence InstructionSequence { get; set; }
        BaseCBracketKeyword CBracketKeyword { get; set; }

        public BlockBlockInstruction(
            BaseOBracketKeyword OBracketKeyword,
            BaseInstructionSequence InstructionSequence,
            BaseCBracketKeyword CBracketKeyword
            ) : base(InstructionSequence.Line, InstructionSequence.Col, InstructionSequence.Text)
        {
            this.OBracketKeyword = OBracketKeyword;
            this.InstructionSequence = InstructionSequence;
            this.CBracketKeyword = CBracketKeyword;
        }

        public override string Assembly()
        {
            return InstructionSequence.Assembly();
        }
    }
}
