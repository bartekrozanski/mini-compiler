﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseIfInstruction : BaseNode
    {
        public BaseIfInstruction(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class IfIfInstruction : BaseIfInstruction
    {
        BaseIfKeyword IfKeyword { get; set; }
        BaseOParentKeyword OParentKeyword { get; set; }
        BaseAssignmentExpression AssignmentExpression { get; set; }

        BaseCParentKeyword CParentKeyword {get; set;}
        BaseInstruction Instruction { get; set; }

        public IfIfInstruction(
            BaseIfKeyword IfKeyword,
            BaseOParentKeyword OParentKeyword,
            BaseAssignmentExpression AssignmentExpression,
            BaseCParentKeyword CParentKeyword,
            BaseInstruction Instruction
            ) : base(AssignmentExpression.Line, AssignmentExpression.Col, AssignmentExpression.Text)
        {
            this.IfKeyword = IfKeyword;
            this.OParentKeyword = OParentKeyword;
            this.AssignmentExpression = AssignmentExpression;
            this.CParentKeyword = CParentKeyword;
            this.Instruction = Instruction;
        }

        public override string Assembly()
        {
            string falseOutcomeLabel = LabelProvider.GetNewLabel();
            string code = string.Empty;
            code += AssignmentExpression.Assembly();
            code += $"brfalse {falseOutcomeLabel}\n";
            code += Instruction.Assembly();
            code += $"{falseOutcomeLabel}: ";
            if (AssignmentExpression.GetTypeEnum() != TypeEnum.Bool)
            {
                ErrorMessages.AddInvalidExpressionType("if", "(bool)", AssignmentExpression.GetTypeEnum().ToString(), IfKeyword.Line);
            }
            return code;
        }
    }

    public class IfElseIfInstruction : BaseIfInstruction
    {

        BaseIfKeyword IfKeyword { get; set; }
        BaseOParentKeyword OParentKeyword { get; set; }
        BaseAssignmentExpression AssignmentExpression { get; set; }

        BaseCParentKeyword CParentKeyword { get; set; }
        BaseInstruction InstructionTrue { get; set; }

        BaseElseKeyword ElseKeyword { get; set; }
        BaseInstruction InstructionFalse { get; set; }

        public IfElseIfInstruction(
            BaseIfKeyword IfKeyword,
            BaseOParentKeyword OParentKeyword,
            BaseAssignmentExpression AssignmentExpression,
            BaseCParentKeyword CParentKeyword,
            BaseInstruction InstructionTrue,
            BaseElseKeyword ElseKeyword,
            BaseInstruction InstructionFalse
            ) : base(AssignmentExpression.Line, AssignmentExpression.Col, AssignmentExpression.Text)
        {
            this.IfKeyword = IfKeyword;
            this.OParentKeyword = OParentKeyword;
            this.AssignmentExpression = AssignmentExpression;
            this.CParentKeyword = CParentKeyword;
            this.InstructionTrue = InstructionTrue;
            this.ElseKeyword = ElseKeyword;
            this.InstructionFalse = InstructionFalse;
        }

        public override string Assembly()
        {
            string falseOutcomeLabel = LabelProvider.GetNewLabel();
            string trueOutcomeLabel = LabelProvider.GetNewLabel();
            string code = string.Empty;
            code += AssignmentExpression.Assembly();
            code += $"brfalse {falseOutcomeLabel}\n";
            code += InstructionTrue.Assembly();
            code += $"br {trueOutcomeLabel}\n";
            code += $"{falseOutcomeLabel}: ";
            code += InstructionFalse.Assembly();
            code += $"{trueOutcomeLabel}: ";
            if(AssignmentExpression.GetTypeEnum() != TypeEnum.Bool)
            {
                ErrorMessages.AddInvalidExpressionType("if", "(bool)", AssignmentExpression.GetTypeEnum().ToString(), IfKeyword.Line);
            }
            return code;
        }
    }
}
