﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseReturnInstruction : BaseNode
    {
        public BaseReturnInstruction(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class ReturnReturnInstruction : BaseReturnInstruction
    {
        BaseReturnKeyword ReturnKeyword { get; set; }
        BaseSemicolonKeyword SemicolonKeyword { get; set; }
        public ReturnReturnInstruction(
            BaseReturnKeyword ReturnKeyword,
            BaseSemicolonKeyword SemicolonKeyword
            ) : base(ReturnKeyword.Line, ReturnKeyword.Col, ReturnKeyword.Text)
        {
            this.ReturnKeyword = ReturnKeyword;
            this.SemicolonKeyword = SemicolonKeyword;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            code += $"br {LabelProvider.LastLine}\n";
            return code;
        }
    }
}
