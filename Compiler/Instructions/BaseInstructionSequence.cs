﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseInstructionSequence : BaseNode
    {
        public BaseInstructionSequence(int line, int col, string text) : base(line, col, text)
        {
        }

        public override string Assembly()
        {
            return base.Assembly();
        }
    }

    public class EmptyInstructionSequence : BaseInstructionSequence
    {
        public EmptyInstructionSequence(int line, int col, string text) : base(line, col, text)
        {
        }

        public override string Assembly()
        {
            return string.Empty;
        }
    }

    public class InstructionInstructionSequence : BaseInstructionSequence
    {
        BaseInstructionSequence InstructionSequence { get; set; }
        BaseInstruction Instruction { get; set; }
        public InstructionInstructionSequence(
            BaseInstructionSequence InstructionSequence,
            BaseInstruction Instruction
            ) : base(Instruction.Line, Instruction.Col, Instruction.Text)
        {
            this.InstructionSequence = InstructionSequence;
            this.Instruction = Instruction;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            code += InstructionSequence.Assembly();
            code += Instruction.Assembly();
            return code;
        }
    }
}
