﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{

    public abstract class BaseValueType : BaseTypeNode
    {
        public abstract string LoadConst { get; }
        public abstract string Type { get; }
        public abstract string TypeModuleName { get; }

        public abstract string DefaultValue { get; }
        public BaseValueType(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class IntValueType : BaseValueType
    {
        public IntValueType(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }

        public IntValueType(int line, int col, string text) : base(line, col, text)
        {
        }

        public override string LoadConst => "ldc.i4";
        public override string Type => "int32";
        public override string TypeModuleName => "Int32";
        public override string DefaultValue => "0";

        public override string Assembly()
        {
            throw new NotImplementedException();
        }

        protected override void UpdateTypeEnum()
        {
            typeEnum = TypeEnum.Int;
        }
    }
    public class BoolValueType : BaseValueType
    {
        public BoolValueType(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }

        public BoolValueType(int line, int col, string text) : base(line, col, text)
        {
        }

        public override string LoadConst=> "ldc.i4";
        public override string Type => "bool";
        public override string TypeModuleName => "Boolean";
        public override string DefaultValue => "0";

        public override string Assembly()
        {
            throw new NotImplementedException();
        }
        protected override void UpdateTypeEnum()
        {
            typeEnum = TypeEnum.Bool;
        }
    }
    public class DoubleValueType : BaseValueType
    {

        public DoubleValueType(
                LexicalInfo lexicalInfo
                ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }

        public DoubleValueType(int line, int col, string text) : base(line, col, text)
        {
        }

        public override string LoadConst => "ldc.r8";
        public override string Type => "float64";
        public override string TypeModuleName => "Double";
        public override string DefaultValue => "0.0";

        public override string Assembly()
        {
            throw new NotImplementedException();
        }
        protected override void UpdateTypeEnum()
        {
            typeEnum = TypeEnum.Double;
        }
    }
}
