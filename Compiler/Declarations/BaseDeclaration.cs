﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseDeclaration : BaseNode
    {
        public BaseDeclaration(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class DeclarationDeclaration : BaseDeclaration
    {
        BaseValueType ValueType { get; set; }
        BaseIdKeyword IdKeyword { get; set; }
        BaseSemicolonKeyword SemicolonKeyword { get; set; }
        public DeclarationDeclaration(
            BaseValueType ValueType,
            BaseIdKeyword IdKeyword,
            BaseSemicolonKeyword SemicolonKeyword
            ) : base(IdKeyword.Line, IdKeyword.Col, IdKeyword.Text)
        {
            this.ValueType = ValueType;
            this.IdKeyword = IdKeyword;
            this.SemicolonKeyword = SemicolonKeyword;
        }

        public override string Assembly()
        {
            int newId = Context.Count;
            string code = string.Empty;
            code += $"[{newId}] ";
            code += ValueType.Type;
            code += $" id_{IdKeyword.Text}";
            if(Context.Exists(IdKeyword.Text))
            {
                ErrorMessages.AddDuplicatedDeclaration(IdKeyword.Text, IdKeyword.Line);
            }
            else
            {
                Context.AddVariable(
                    IdKeyword.Text,
                    newId,
                    ValueType.Type
                    );
            }
            return code;
        }
    }
    public class SyntaxErrorMissingSemicolonDeclaration : BaseDeclaration
    {

        public SyntaxErrorMissingSemicolonDeclaration(LexicalInfo lexicalInfo) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
            ErrorMessages.AddSyntaxErrorMissingSemicolon(Line);
        }
    }
}
