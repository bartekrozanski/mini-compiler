﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseDeclarationSequence : BaseNode
    {
        public BaseDeclarationSequence(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class EmptyDeclarationSequence : BaseDeclarationSequence
    {
        public EmptyDeclarationSequence(int line, int col, string text) : base(line, col, text)
        {
        }

        public override string Assembly()
        {
            return string.Empty;
        }
    }

    public class DeclarationDeclarationSequence : BaseDeclarationSequence
    {
        BaseDeclarationSequence DeclarationSequence { get; set; }
        BaseDeclaration Declaration { get; set; }
        public DeclarationDeclarationSequence(
            BaseDeclarationSequence DeclarationSequence,
            BaseDeclaration Declaration
            ) : base(Declaration.Line, Declaration.Col, Declaration.Text)
        {
            this.DeclarationSequence = DeclarationSequence;
            this.Declaration = Declaration;
        }

        public override string Assembly()
        {
            string code = string.Empty;
            code += DeclarationSequence.Assembly();
            if(!string.IsNullOrEmpty(code)) code += ",\n";
            code += Declaration.Assembly();
            return code;
        }
    }
}
