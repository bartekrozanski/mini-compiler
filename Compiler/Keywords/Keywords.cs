﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCompiler
{
    public abstract class BaseProgramKeyword : BaseNode
    {
        public BaseProgramKeyword(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class ProgramKeyword : BaseProgramKeyword
    {
        public ProgramKeyword(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }
    }

    public abstract class BaseEof : BaseNode
    {
        public BaseEof(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class Eof : BaseEof
    {
        public Eof(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }
    }
    public abstract class BaseOBracketKeyword : BaseNode
    {
        public BaseOBracketKeyword(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class OBracketKeyword : BaseOBracketKeyword
    {
        public OBracketKeyword(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }
    }
    public abstract class BaseCBracketKeyword : BaseNode
    {
        public BaseCBracketKeyword(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class CBracketKeyword : BaseCBracketKeyword
    {
        public CBracketKeyword(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }
    }

    public abstract class BaseReturnKeyword : BaseNode
    {
        public BaseReturnKeyword(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class ReturnKeyword : BaseReturnKeyword
    {
        public ReturnKeyword(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }
    }

    public abstract class BaseIdKeyword : BaseTypeNode
    {
        public abstract int IntId { get; }
        public abstract BaseValueType ValueType { get; }

        public BaseIdKeyword(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class IdKeyword : BaseIdKeyword
    {
        public IdKeyword(
          LexicalInfo lexicalInfo
          ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }

        public override int IntId => GetIntId();
        private int GetIntId()
        {
            if(Context.Exists(Text))
            {
                return Context.GetIntId(Text);
            }
            else
            {
                ErrorMessages.AddUndeclaredVariable(Text, Line);
                return -1;   
            }
        }
        private BaseValueType GetValueType()
        {
            if (Context.Exists(Text))
            {
                return Context.GetValueType(Text);
            }
            else
            {
                return Context.DoubleValueTypeNode;
            }
        }

        public override BaseValueType ValueType => GetValueType();

        public override string Assembly()
        {
            return $"ldloc {IntId}\n";
        }

        protected override void UpdateTypeEnum()
        {
            typeEnum = ValueType.GetTypeEnum();
        }
    }

    public class BaseStringKeyword : BaseNode
    {
        public BaseStringKeyword(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class StringKeyword : BaseStringKeyword
    {
        string Value { get; set; }
        public StringKeyword(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
            Value = lexicalInfo.Text;
        }

        public override string Assembly()
        {
            return $"ldstr {Value}\n";
        }
    }

    public class BaseSemicolonKeyword : BaseNode
    {
        public BaseSemicolonKeyword(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class SemicolonKeyword : BaseSemicolonKeyword
    {
        public SemicolonKeyword(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }
    }

    public class BaseElseKeyword : BaseNode
    {
        public BaseElseKeyword(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class ElseKeyword : BaseElseKeyword
    {
        public ElseKeyword(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }
    }

    public class BaseOParentKeyword : BaseNode
    {
        public BaseOParentKeyword(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class OParentKeyword : BaseOParentKeyword
    {
        public OParentKeyword(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }
    }

    public class BaseCParentKeyword : BaseNode
    {
        public BaseCParentKeyword(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class CParentKeyword : BaseCParentKeyword
    {
        public CParentKeyword(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }
    }

    public class BaseIfKeyword : BaseNode
    {
        public BaseIfKeyword(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class IfKeyword : BaseIfKeyword
    {
        public IfKeyword(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }
    }

    public class BaseWhileKeyword : BaseNode
    {
        public BaseWhileKeyword(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class WhileKeyword : BaseWhileKeyword
    {
        public WhileKeyword(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }
    }

    public abstract class BaseReadKeyword : BaseNode
    {
        public BaseReadKeyword(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class ReadKeyword : BaseReadKeyword
    {
        public ReadKeyword(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }

        public override string Assembly()
        {
            string code = string.Empty;
            code += "call string [System.Console]System.Console::ReadLine()\n";
            return code;
        }
    }

    public abstract class BaseWriteKeyword : BaseNode
    {
        public BaseWriteKeyword(int line, int col, string text) : base(line, col, text)
        {
        }
    }

    public class WriteKeyword : BaseWriteKeyword
    {
        public WriteKeyword(
            LexicalInfo lexicalInfo
            ) : base(lexicalInfo.Line, lexicalInfo.Col, lexicalInfo.Text)
        {
        }
    }
}
