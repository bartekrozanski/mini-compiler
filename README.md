# Description

Compiler for fictive language "Mini". It compiles .mini files to CIL.

Written in C#. Uses Gplex and gppg for generating scanner and parser.

# Examples
Helloworld input:
```
program
{
	write "Hello world\n";
	return;
}
```
Output:
```
.assembly MiniCompiler {}
.assembly extern mscorlib {}
.method static void Main()
{
  .entrypoint
  .locals init (
    
    )
  ldstr "Hello world\n"
  call void [mscorlib]System.Console::Write(string)
  br END_0
  END_0: ret
  }

```
For more examples, checkout `Compiler/examples` directory.

# Building the project
Clone this repository, open .sln file with VisualStudio and build the project.
